var mysql      = require('mysql');
var config = require('./config.js');
var Promise = require("bluebird");
var connection = mysql.createConnection(config);

var ExecuteQuery = function(query,callback){
  return new Promise(function(resolve,reject){
    connection.connect();

    connection.query(query, function (error, results, fields) {
      if (error){
        reject(error);
      }
      else{
        resolve(results);
      }
    });

    connection.end();
  })
}


module.exports = ExecuteQuery;
